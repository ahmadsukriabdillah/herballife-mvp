package com.herballife.main.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.herballife.main.main.MainActivity;
import com.herballife.main.R;

public class Splashscreen extends Activity implements SplashContract.View
{
	 SplashContract.Presenter presenter;
	 ProgressBar bar;
	 TextView txt;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		bar=(ProgressBar)findViewById(R.id.prog);
		txt=(TextView)findViewById(R.id.load);
		presenter = new SplashPresenter(this);
	}
		
	public void onStart() 
	{
	 	super.onStart();
		presenter.start();
	}

	@Override
	public void setPresenter(SplashContract.Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		presenter.destroy();
	}

	@Override
	public void updateLoading(int state) {
		bar.setProgress(state);
	}

	@Override
	public void navigateToHome() {
		startActivity(new Intent(getApplicationContext(),MainActivity.class));
		finish();
	}

	@Override
	public void updateTextLoading(String s) {
		txt.setText(s);
	}
}