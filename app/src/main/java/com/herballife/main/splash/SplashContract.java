package com.herballife.main.splash;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

/**
 * Created by sx on 17/08/17.
 */

public interface SplashContract {
    interface View extends BaseView<Presenter>{
        public void updateLoading(int state);
        public void navigateToHome();
        void updateTextLoading(String s);
    }
    interface Presenter extends BasePresenter{

    }
}
