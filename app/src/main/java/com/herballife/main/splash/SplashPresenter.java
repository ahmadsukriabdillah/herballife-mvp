package com.herballife.main.splash;

import android.os.Handler;
import android.os.Message;


/**
 * Created by sx on 17/08/17.
 */

public class SplashPresenter implements SplashContract.Presenter {


    SplashContract.View view;
    private int total = 0;
    Handler handler=new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            total=total+20;
            String perc=String.valueOf(total).toString();
            view.updateLoading(total);
            view.updateTextLoading(perc+"% Completed");
        }
    };
    Thread background=new Thread(new Runnable() {
        public void run() {
            try {
                for (int i=0;i<5;i++) {
                    Thread.sleep(1000);
                    handler.sendMessage(handler.obtainMessage());
                }
                view.navigateToHome();
            }
            catch (Throwable t) {
                t.printStackTrace();
            }
        }

    });

    public SplashPresenter(SplashContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        background.start();
        view.setPresenter(this);
    }

    @Override
    public void destroy() {
    }
}
