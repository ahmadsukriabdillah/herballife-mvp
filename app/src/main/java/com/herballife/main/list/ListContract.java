package com.herballife.main.list;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

import java.util.*;
import java.util.List;

/**
 * Created by sx on 17/08/17.
 */

public interface ListContract {
    interface View extends BaseView<Presenter>{
        void navigateToCari();

        void navigateToDetail(String penyakit);

        void setDataToAdapter(ArrayList<String> dataPenyakit);

    }
    interface Presenter extends BasePresenter{

        void caripenyakit();

        void navigateToDetailPenyakit(String penyakit);
    }
}
