package com.herballife.main.list;

import com.herballife.main.PenyakitRepository;

/**
 * Created by sx on 17/08/17.
 */

public class ListPresenter implements ListContract.Presenter {
    ListContract.View view;
    PenyakitRepository penyakitRepository;

    public ListPresenter(ListContract.View view, PenyakitRepository penyakitRepository) {
        this.view = view;
        this.penyakitRepository = penyakitRepository;
    }

    @Override
    public void start() {
        this.view.setPresenter(this);
        this.view.setDataToAdapter(penyakitRepository.getDataPenyakit());
    }

    @Override
    public void destroy() {

    }

    @Override
    public void caripenyakit() {
        view.navigateToCari();
    }

    @Override
    public void navigateToDetailPenyakit(String penyakit) {
        view.navigateToDetail(penyakit);
    }
}
