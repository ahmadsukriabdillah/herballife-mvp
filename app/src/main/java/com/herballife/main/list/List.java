package com.herballife.main.list;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.herballife.main.PenyakitRepository;
import com.herballife.main.R;
import com.herballife.main.SQLiteDBHelper;
import com.herballife.main.cari_penyakit.Cari_Penyakit;
import com.herballife.main.detail_penyakit.Detail;

public class List extends Activity implements ListContract.View
{
	ListContract.Presenter presenter;
	public ListView list;
	Button button;
	int checked = -1;
	ArrayList<String> listitem1= new ArrayList<String>();
	ArrayAdapter<String> adapter;
	 protected void onCreate(Bundle savedInstanceState)
	 {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.list);
	       list = (ListView) findViewById(R.id.listpenyakit);
	       adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listitem1);
	       list.setAdapter(adapter);
	       list.setOnItemClickListener(new OnItemClickListener(){
	        	

				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) 
				{
					checked = arg2;
					String penyakit = listitem1.get(checked);
					presenter.navigateToDetailPenyakit(penyakit);
					
				}
	        	
	        });
	       button = (Button)findViewById(R.id.tombol_cari);
	       button.setOnClickListener(new OnClickListener() {
			   @Override
			   public void onClick(View view) {
				   presenter.caripenyakit();
			   }
		   });
	       presenter = new ListPresenter(this,new PenyakitRepository(getApplicationContext()));
		 presenter.start();
	  
  }


	@Override
	public void setPresenter(ListContract.Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void navigateToCari() {
		Intent tombol = new Intent(this, Cari_Penyakit.class);
		startActivity(tombol);
	}

	@Override
	public void navigateToDetail(String penyakit) {
		Intent tambah = new Intent(List.this,Detail.class);
		tambah.putExtra("kirim_penyakit",penyakit);
		startActivity(tambah);
	}

	@Override
	public void setDataToAdapter(ArrayList<String> dataPenyakit) {
		listitem1 = dataPenyakit;
		adapter.notifyDataSetChanged();
	}
}
