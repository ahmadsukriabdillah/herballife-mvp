package com.herballife.main.cari_penyakit;

import android.text.Editable;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

/**
 * Created by sx on 17/08/17.
 */

public interface CariPenyakitContract {
    interface View extends BaseView<Presenter>{
        void navigateToDetail(String namapenyakit);
        void setAdapterDataPencarian(String[] data);
    }
    interface Presenter extends BasePresenter{

        void setTextSelection(String text);

        void onClickListItem();
    }
}
