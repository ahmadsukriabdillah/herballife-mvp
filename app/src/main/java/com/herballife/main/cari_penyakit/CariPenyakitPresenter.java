package com.herballife.main.cari_penyakit;

/**
 * Created by sx on 17/08/17.
 */

public class CariPenyakitPresenter implements CariPenyakitContract.Presenter {
    CariPenyakitContract.View view;
    PenyakitRepository penyakitRepository;
    private String selection;

    public CariPenyakitPresenter(CariPenyakitContract.View view) {
        this.view = view;
        this.penyakitRepository = new PenyakitRepository();
    }

    @Override
    public void start() {
        this.view.setPresenter(this);
        this.view.setAdapterDataPencarian(penyakitRepository.getItems());
    }

    @Override
    public void destroy() {

    }

    @Override
    public void setTextSelection(String text) {
        this.selection = text;
    }

    @Override
    public void onClickListItem() {
        this.view.navigateToDetail(this.selection);
    }
}
