package com.herballife.main.cari_penyakit;

/**
 * Created by sx on 17/08/17.
 */

public class PenyakitRepository {
    private  String[] items = { "Batuk Pada Anak", "Sakit Perut","Diare","Mual",
            "Kembung","Wasir","Biduran",
            "Demam","Step","Kencing Batu","Radang Paru-paru","Asma","Mimisan","Hepatitis",
            "Prostat","Keputihan","Diabetes Melitus","Bisul","Jerawat","Gatal berupa bintik-bintik merah bergelembung air",
            "Gatal pada bekas luka yang sudah kering","Nyeri haid","Haid bau anyir","Batuk Kering","Sariawan","Campak",
            "Borok","Jantung Lemah","Gangguan saraf","Rematik","Demam Pada Anak","Masuk Angin","Disentri","Hipertensi",
            "Diabetes","Kutu Air","Sakit Kepala","Flu","Bronkitis","Cacingan","Migrain","Maag","Cantengan","Osteoporosis"};

    public PenyakitRepository() {
    }

    public String[] getItems() {
        return items;
    }
}
