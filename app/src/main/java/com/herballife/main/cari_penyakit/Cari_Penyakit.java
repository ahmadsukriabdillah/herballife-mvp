package com.herballife.main.cari_penyakit;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.herballife.main.detail_penyakit.Detail;
import com.herballife.main.R;


public class Cari_Penyakit  extends Activity implements CariPenyakitContract.View{
	CariPenyakitContract.Presenter presenter;
	TextView selection;
	AutoCompleteTextView edit;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cari_penyakit);

       
        selection = (TextView) findViewById(R.id.selection);
        edit = (AutoCompleteTextView) findViewById(R.id.edit);
        edit.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
				presenter.setTextSelection(edit.getText().toString());
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});

        edit.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
					presenter.onClickListItem();
			}
        	
        });
        presenter = new CariPenyakitPresenter(this);
        presenter.start();
    }



	@Override
	public void setPresenter(CariPenyakitContract.Presenter presenter) {
        this.presenter = presenter;
	}

	@Override
	public void navigateToDetail(String namapenyakit) {
		Intent tambah = new Intent(Cari_Penyakit.this,Detail.class);
		tambah.putExtra("kirim_penyakit",namapenyakit);
		startActivity(tambah);
	}

	@Override
	public void setAdapterDataPencarian(String[] data) {
		edit.setAdapter(new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, data));
	}
}

