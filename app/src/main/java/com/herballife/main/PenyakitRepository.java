package com.herballife.main;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.herballife.main.SQLiteDBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sx on 17/08/17.
 */

public class PenyakitRepository {
    public static SQLiteDatabase db;
    SQLiteDBHelper helper;

    public PenyakitRepository(Context context) {
        try {
            helper = new SQLiteDBHelper(context);
            db = helper.getDb();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
    public String[] getBahanDanTutor(String cek){
        String sql = "select * from data_penyakit where Nama ='"+cek+"'";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        int index1 = c.getColumnIndex("BahanObat");
        int index2 = c.getColumnIndex("Tutorial");
        String[] res = new String[2];
        res[0] = c.getString(index1);
        res[1] = c.getString(index2);
        return res;
    }
    public ArrayList<String> getDataPenyakit(){
        ArrayList<String> list = new ArrayList<>();
        String sql = "select * from data_penyakit ";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        int i = 0;
        while (!c.isAfterLast())
        {
            int index = c.getColumnIndex("Nama");
            String temp = c.getString(index);
            list.add(temp);

            i++;
            c.moveToNext();
        }
        return list;
    }
}
