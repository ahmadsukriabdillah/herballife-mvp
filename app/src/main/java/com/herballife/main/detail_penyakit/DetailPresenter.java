package com.herballife.main.detail_penyakit;

import com.herballife.main.PenyakitRepository;

/**
 * Created by sx on 17/08/17.
 */

public class DetailPresenter implements DetailPenyakitContract.Presenter {
    DetailPenyakitContract.View view;
    PenyakitRepository repository;

    public DetailPresenter(DetailPenyakitContract.View view, PenyakitRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void start() {
        view.setPresenter(this);

    }

    @Override
    public void destroy() {

    }

    @Override
    public void iihatCatalog() {
        view.navigateToCatalog();
    }

    @Override
    public void cariDetail(String cek) {
        String[] res = repository.getBahanDanTutor(cek);
        view.setBahan(res[0]);
        view.setTutor(res[1]);
    }
}
