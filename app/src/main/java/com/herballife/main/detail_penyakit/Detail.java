package com.herballife.main.detail_penyakit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.herballife.main.PenyakitRepository;
import com.herballife.main.list_catalog.List_catalog;
import com.herballife.main.R;

public class Detail extends Activity  implements DetailPenyakitContract.View
{
    DetailPenyakitContract.Presenter presenter;

	TextView nama;
	TextView bahan;
	TextView tutor;
	Button button;
	String cek;
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_penyakit);
        Intent qwe = getIntent();
		cek = qwe.getStringExtra("kirim_penyakit");

           nama = (TextView)findViewById(R.id.daftar_penyakit);
           bahan =(TextView)findViewById(R.id.bahan);
           button = (Button)findViewById(R.id.button1);
           tutor = (TextView)findViewById(R.id.tutorial);
           nama.setText(cek);
           button.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View view) {
                   presenter.iihatCatalog();
               }
           });
        presenter = new DetailPresenter(this,new PenyakitRepository(getApplicationContext()));
        presenter.cariDetail(cek);
	
	}

    @Override
    public void setPresenter(DetailPenyakitContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setBahan(String a) {
        bahan.setText("Bahan : " +a);

    }

    @Override
    public void setTutor(String a) {
        tutor.setText("Cara Menggunakan : "+a);
    }

    @Override
    public void navigateToCatalog() {
        Intent tombol = new Intent(this, List_catalog.class);
        startActivity(tombol);
    }
}
