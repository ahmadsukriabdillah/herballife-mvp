package com.herballife.main.detail_penyakit;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

/**
 * Created by sx on 17/08/17.
 */

public interface DetailPenyakitContract {
    interface Presenter extends BasePresenter{

        void iihatCatalog();

        void cariDetail(String cek);
    }
    interface View extends BaseView<Presenter>{
        void setBahan(String bahan);
        void setTutor(String tutor);

        void navigateToCatalog();
    }
}
