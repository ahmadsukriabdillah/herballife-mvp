package com.herballife.main.list_catalog;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sx on 17/08/17.
 */

public interface ListCatalogContract {
    interface View extends BaseView<Presenter> {
        void navigateToDetailCatalog(String string);
        void populateListItem(ArrayList<String> strings);
    }
    interface Presenter extends BasePresenter {

        void navigateToDetailCatalog(String tumbuhan);
    }
}
