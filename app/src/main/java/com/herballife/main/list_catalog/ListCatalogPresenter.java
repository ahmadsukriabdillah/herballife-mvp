package com.herballife.main.list_catalog;

import com.herballife.main.CatalogRepository;

/**
 * Created by sx on 17/08/17.
 */

public class ListCatalogPresenter implements ListCatalogContract.Presenter {
    ListCatalogContract.View view;
    CatalogRepository catalogRepository;

    public ListCatalogPresenter(ListCatalogContract.View view, CatalogRepository catalogRepository) {
        this.view = view;
        this.catalogRepository = catalogRepository;
    }

    @Override
    public void start() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void navigateToDetailCatalog(String tumbuhan) {

    }
}
