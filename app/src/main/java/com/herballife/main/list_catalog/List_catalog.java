package com.herballife.main.list_catalog;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.herballife.main.CatalogRepository;
import com.herballife.main.R;
import com.herballife.main.detail_katalog.Detail_katalog;

import java.util.ArrayList;

public class List_catalog extends Activity implements ListCatalogContract.View
{
	ListCatalogContract.Presenter presenter;
	public ListView list;
	int checked = -1;
	ArrayList<String > listitem1 = new ArrayList<>();
	ArrayAdapter<String> adapter;
	 protected void onCreate(Bundle savedInstanceState)
	 {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.list_katalog);


				list = (ListView) findViewById(R.id.list_tumbuhan);
				adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listitem1);

				list.setAdapter(adapter);
				list.setOnItemClickListener(new OnItemClickListener()
				{


					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
					{
						checked = arg2;
						String tumbuhan = listitem1.get(checked);

						presenter.navigateToDetailCatalog(tumbuhan);
					}

				});

			presenter = new ListCatalogPresenter(this, new CatalogRepository(getApplicationContext()));
		 presenter.start();


  }

	@Override
	public void setPresenter(ListCatalogContract.Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void navigateToDetailCatalog(String string) {
		Intent tambah = new Intent(List_catalog.this,Detail_katalog.class);
		tambah.putExtra("kirim_tumbuhan",string);
		startActivity(tambah);
	}

	@Override
	public void populateListItem(ArrayList<String> strings) {
		this.listitem1 = strings;
		this.adapter.notifyDataSetChanged();
	}
}