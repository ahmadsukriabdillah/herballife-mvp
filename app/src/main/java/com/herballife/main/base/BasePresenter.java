package com.herballife.main.base;

/**
 * Created by sx on 17/08/17.
 */

public interface BasePresenter {
    void start();

    void destroy();
}
