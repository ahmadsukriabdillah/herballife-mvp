package com.herballife.main.base;

/**
 * Created by sx on 17/08/17.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
