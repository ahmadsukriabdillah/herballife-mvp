package com.herballife.main.main;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

/**
 * Created by sx on 17/08/17.
 */

public interface MainActivityContract {
    interface View extends BaseView<Presenter>{
        public void onClickSerchButtnon();
        public void onClickCatalogButtnon();
        public void onClickHelpButtnon();
        public void onClickExitButtnon();

    }
    interface Presenter extends BasePresenter{
        void navigateToExit();

        void navigateToHelp();

        void navigateToCatalog();

        void navigateToSerch();
    }
}
