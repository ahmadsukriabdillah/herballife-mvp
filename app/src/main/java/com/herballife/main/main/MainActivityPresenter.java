package com.herballife.main.main;

/**
 * Created by sx on 17/08/17.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {
    MainActivityContract.View view;

    public MainActivityPresenter(MainActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        this.view.setPresenter(this);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void navigateToExit() {
        view.onClickExitButtnon();
    }

    @Override
    public void navigateToHelp() {
        view.onClickHelpButtnon();
    }

    @Override
    public void navigateToCatalog() {
        view.onClickCatalogButtnon();
    }

    @Override
    public void navigateToSerch() {
        view.onClickSerchButtnon();
    }
}
