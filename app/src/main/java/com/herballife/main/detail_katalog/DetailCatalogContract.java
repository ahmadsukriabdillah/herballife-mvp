package com.herballife.main.detail_katalog;

import android.graphics.Bitmap;

import com.herballife.main.base.BasePresenter;
import com.herballife.main.base.BaseView;

/**
 * Created by sx on 17/08/17.
 */

public interface DetailCatalogContract {
    interface View extends BaseView<Presenter> {
        void setGambar(Bitmap bitmap);
        void setKegunaan(String kegunaan);
    }
    interface Presenter extends BasePresenter {
        void cari(String cari);
    }
}
