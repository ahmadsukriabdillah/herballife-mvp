package com.herballife.main.detail_katalog;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.herballife.main.CatalogRepository;
import com.herballife.main.R;

public class Detail_katalog extends Activity implements DetailCatalogContract.View
{
    DetailCatalogContract.Presenter presenter;

	TextView nama;
	TextView guna;
	ImageView gambar;
	String cek;
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_katalog);
        Intent qwe = getIntent();
		cek = qwe.getStringExtra("kirim_tumbuhan");
        nama = (TextView)findViewById(R.id.nama_tumbuhan);
        guna = (TextView)findViewById(R.id.kegunaan);
        gambar = (ImageView)findViewById(R.id.gambar);
        nama.setText(cek);
        presenter = new DetailKatalogPresenter(new CatalogRepository(getApplicationContext()),this);
        presenter.cari(cek);

	}


    @Override
    public void setPresenter(DetailCatalogContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setGambar(Bitmap bitmap) {
        gambar.setImageBitmap(bitmap);
    }

    @Override
    public void setKegunaan(String kegunaan) {
        guna.setText("Kegunaan : "+kegunaan);
    }
}