package com.herballife.main.detail_katalog;

import com.herballife.main.CatalogRepository;

/**
 * Created by sx on 17/08/17.
 */

public class DetailKatalogPresenter implements DetailCatalogContract.Presenter{
    CatalogRepository catalogRepository;
    DetailCatalogContract.View view;

    public DetailKatalogPresenter(CatalogRepository catalogRepository, DetailCatalogContract.View view) {
        this.catalogRepository = catalogRepository;
        this.view = view;
    }

    @Override
    public void start() {
        this.view.setPresenter(this);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void cari(String cari) {
        view.setGambar(catalogRepository.getGambar(cari));
        view.setKegunaan(catalogRepository.getKegunaan(cari));

    }
}
